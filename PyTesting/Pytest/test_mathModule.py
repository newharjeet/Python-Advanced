import mathModule
import pytest
import sys

# @pytest.mark.skip(reason="Just want to test the skip feature")
@pytest.mark.skipif(sys.version_info[0] == 3, reason="Skip if version is 3.x")
def test_add():
    add_result = mathModule.add(2,3)
    assert add_result == 5

def test_sub():
    sub_result = mathModule.sub(6,3)
    assert sub_result == 3

@pytest.mark.dummytest
def test_dummy():
    assert True
