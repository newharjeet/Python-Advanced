import argparse

def add(num1, num2):
    return(num1 + num2)

def sub(num1, num2):
    return(num1 - num2)   

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    
    parser.add_argument("firstNumber", help="First number", type=int)
    parser.add_argument("secondNumber", help="Second number", type=int)
    parser.add_argument("operation", help="Operation", choices=["add", "sub"])

    args = parser.parse_args()
    if(args.operation == "add"):
        result = add(args.firstNumber, args.secondNumber)
        print("Result:", result)
    if(args.operation == "sub"):
        result = sub(args.firstNumber, args.secondNumber)
        print("Result:", result)
