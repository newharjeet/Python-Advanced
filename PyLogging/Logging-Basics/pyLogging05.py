import argparse
import logging

logging.basicConfig(level=logging.INFO,
                    format="%(asctime)s - %(levelname)s - %(message)s")

def mul(num1, num2):
    mul_result = num1 * num2
    print("Result: {}".format(mul_result))
    logging.info("Multiplication result calculated.")

def div(num1, num2):
    div_result = num1 / num2
    print("Result: {}".format(div_result))
    logging.info("Divison result calculated.")

def main(num1, num2):
    mul(num1, num2)
    logging.info("Multiplication implemented successfully.")
    div(num1, num2)
    logging.info("Division implemented successfully.")
    
if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument("firstNumber", type=int, help="First Number")
    parser.add_argument("secondNumber", type=int, help="Second Number")
    
    args = parser.parse_args()
    main(args.firstNumber, args.secondNumber)
