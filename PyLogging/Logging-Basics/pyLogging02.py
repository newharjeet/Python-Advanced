import logging
logging.basicConfig(level=logging.NOTSET, format="%(asctime)s - %(levelname)s - %(message)s")

logging.debug("Debug log.")
logging.info("Info log.")
logging.warning("Warning log.")
logging.error("Error log.")
logging.critical("Critical log.")
