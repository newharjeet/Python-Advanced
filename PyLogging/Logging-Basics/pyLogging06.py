import argparse
import logging

logging.basicConfig(level=logging.INFO,
                    format="%(asctime)s - %(levelname)s - %(message)s")

class Employee:

    def __init__(self, name, company):
        self.empName = name
        self.companyName = company
        logging.info("Employee {} created".format(self.empName))

    def showData(self):
        print("Employee name: {}".format(self.empName))
        print("Company name: {}".format(self.companyName))

    def emailGenerator(self):
        return("{}.{}@gmail.com".format(self.empName, self.companyName))

def main():
    emp01 = Employee("Anupam", "Opstree")
    emp02 = Employee("Arpit", "Techprimo")

if __name__ == "__main__":
    main()
