""" Logging messages without append in the file. """

import logging
logging.basicConfig(filename="log_file04.txt", filemode="w", level=logging.ERROR,
                    format="%(asctime)s - %(levelname)s - %(message)s")

logging.debug("Debug log.")
logging.info("Info log.")
logging.warning("Warning log.")
logging.error("Error log.")
logging.critical("Critical log.")
