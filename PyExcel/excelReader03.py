import openpyxl

workbook_obj = openpyxl.load_workbook("appendedExcelFile.xlsx")
active_sheet = workbook_obj.active
    
for row in active_sheet.iter_rows():
    for cell in row:
        print(cell.value, end=" ")
    print()
