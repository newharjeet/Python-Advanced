import openpyxl

workbook_obj = openpyxl.load_workbook("appendedExcelFile.xlsx")
print(workbook_obj.sheetnames) # To get list of all the sheet names of the file.

active_sheet = workbook_obj.active
print(active_sheet) # To get the currently active sheet name.

sheet = workbook_obj["March"] # To use a particular sheet of file.
print(sheet.title) # To get the title of the selected sheet.
