import argparse
import redis

# you could also give the ip address of the machine where redis server is present.
redis_server = redis.Redis("localhost")

def setKeyValue(key, value):
    
    redis_server.set(key, value)
    print("Key-Value pair added successfully.")

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("key", help="Key name")
    parser.add_argument("value", help="Key value")
    args = parser.parse_args()
    setKeyValue(args.key, args.value)
        
