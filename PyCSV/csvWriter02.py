import csv
 
mylist = [{"first_name":"Anupam", "last_name":"Yadav"}, {"last_name":"Singh", "first_name":"Anurag"}]

# Using "," as delimiter
"""with open("Wexample02.csv", "w") as wobj:  

    fnames = ["first_name", "last_name"]
    writer = csv.DictWriter(wobj, fieldnames=fnames)    

    writer.writeheader()
    for row in mylist:
        writer.writerow(row)"""

# Using " " as delimiter 
with open("Wexample02.csv", "w") as wobj:  

    fnames = ["first_name", "last_name"]
    writer = csv.DictWriter(wobj, fieldnames=fnames, delimiter=" ")    

    writer.writeheader()
    writer.writerows(mylist)
