import csv
 
numbers = [[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11, 12]]

"""with open("Wexample01.csv", "w") as wobj:  
    writer = csv.writer(wobj)
    for row in numbers:
        writer.writerow(row)"""

with open("Wexample01.csv", "w") as wobj:  
    writer = csv.writer(wobj)
    writer.writerows(numbers)
