def switchAlt(operation, x, y):
    return {
        "add": lambda x,y: x+y,
        "sub": lambda x,y: x-y,
        "mul": lambda x,y: x*y,
        "div": lambda x,y: x/y
    }.get(operation)(x, y)

def main():
    x = int(input("Enter first number:"))
    y = int(input("Enter second number:"))
    
    choices = ["add", "sub", "mul", "div"]
    print("Operation choices:", choices)
    
    operation = input("Enter the operation from above choices:")
    if(operation in choices):
        print("Result:", switchAlt(operation, x, y))
    else:
        print("Invalid operation choice.")

if __name__ == "__main__":
    main()
