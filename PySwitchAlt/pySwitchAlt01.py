optionsDict = {
    "a" : "This is vowel 'a'.",
    "e" : "This is vowel 'e'.",
    "i" : "This is vowel 'i'.",
    "o" : "This is vowel 'o'.",
    "u" : "This is vowel 'u'."
    }

# Take user input
userInput = input("Enter a character:")

# Message is the default value if there is no keys that matches the given input.
print(optionsDict.get(userInput, "You haven't entered a lowercase vowel."))
